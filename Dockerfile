FROM python:3.7-stretch
LABEL maintainer="julian.moreno_schneider@dfki.de"

RUN apt-get -y update && \
    apt-get upgrade -y && \
    apt-get install -y python3-dev &&\
    apt-get update -y

RUN mkdir translation/ && chmod 777 translation
WORKDIR translation

ADD requirements.txt .
RUN pip3 install -r requirements.txt

ADD loadmodels.py .

RUN pip3 install sentencepiece
RUN python3 loadmodels.py
ADD translation.py .
ADD flaskController.py .

ADD qdoc qdoc

RUN pip3 install nltk
RUN python -c 'import nltk; nltk.download("punkt")'

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

EXPOSE 8080

ENTRYPOINT FLASK_APP=flaskController.py flask run --host=0.0.0.0 --port=8080
#CMD ["/bin/bash"]
