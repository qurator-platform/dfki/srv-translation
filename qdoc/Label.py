import re
import uuid
from typing import List
import rdflib
import json

class Label():

    def __init__(self,
                 id=None,
                 properties=None,
                 **kwargs):
        self.type=[]
        self.id=id
        self.type.append('qont:Label')
        if properties is None:
            self.properties=dict()
        else:
            self.properties=properties
        # self.validate()

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

    def getId(self):
        #if '#' in self.reference_context:
        #    short_context = self.reference_context.split('#')[0]
        #    return short_context + "#offset_" + str(self.offset_ini) + "_" + str(self.offset_end)
        #else:
        #    return self.reference_context + "#offset_" + str(self.offset_ini) + "_" + str(self.offset_end)
        return self.reference_context

    @staticmethod
    def is_label(cxt):
        return isinstance(cxt, Label)

    def add_property(self, key, value):
        self.properties.update(key=value)

    #def update_reference_context(self, reference_context):
    #    self.reference_context = reference_context
    #    self.id = self.getId()

if __name__ == '__main__':
    properties = dict()
    properties['itsrdf:taClassRef']='Location'
    properties['itsrdf:tiIdentRef']='http://dbpedia.de/Berlin'
    label = Label(id='label1',properties=properties)
    print(label.toJSON())

