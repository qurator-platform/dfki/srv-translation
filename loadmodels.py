
from transformers import MarianMTModel, MarianTokenizer

models = {}
tokenizers = {}

def detect_objects():
	model_name = 'Helsinki-NLP/opus-mt-en-ROMANCE'
	model_names = {}
	model_names['ende'] = 'Helsinki-NLP/opus-mt-en-de'
	model_names['deen'] = 'Helsinki-NLP/opus-mt-de-en'
	#model_names['enes'] = 'Helsinki-NLP/opus-mt-en-ROMANCE'
	#model_names['esen'] = 'Helsinki-NLP/opus-mt-ROMANCE-en'
	for k, v in model_names.items():
		print(k, '->', v)
		tokenizer = MarianTokenizer.from_pretrained(v)
		print(tokenizer.supported_language_codes)
		model = MarianMTModel.from_pretrained(v)
		tokenizers[k] = tokenizer
		models[k] = model
detect_objects()
