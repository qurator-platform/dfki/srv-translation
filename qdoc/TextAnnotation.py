import re
import uuid
from typing import List
import rdflib
import json
from qdoc.BaseAnnotation import BaseAnnotation


class TextAnnotation(BaseAnnotation):
    # nif_classes = [nif_ns.Annotation]

    def __init__(self,
                 reference_context,
                 text=None,
                 language=None,
                 types = None,
                 source=None,
                 metadata=None,
                 **kwargs):
        super().__init__(reference_context,
                         source=source,
                         metadata=metadata
                         )
        self.type = []
        self.type.append('qont:TextAnnotation')
        self.text = text
        self.language = language
        #self.id = reference_context
        if types is not None:
            for typ in types:
                self.type.append(typ)
        # self.validate()

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

    @staticmethod
    def is_text_annotation(cxt):
        return isinstance(cxt, TextAnnotation)

    def add_type(self, type):
        self.type.append(type)

    def update_reference_context(self, reference_context):
        self.id = reference_context
        self.reference_context = reference_context

    # def remove_annotation_unit(self, au_uri: str):
    #     self.delattr('nif__annotation_unit', au_uri)
    #     del self.annotation_units[au_uri]

    # def validate(self):
    #     if self.reference_context is not None:
    #         if not NIFContext.is_context(self.reference_context):
    #             raise ValueError(
    #                 'The provided reference context is not compatible with '
    #                 'nif.Context class.')
    #     if self.nif__is_string is not None:
    #         if not isinstance(self.nif__is_string, str):
    #             raise TypeError('is_string value {} should be '
    #                             'a string'.format(self.nif__is_string))
    #         if int(self.nif__begin_index) != 0 or \
    #                 int(self.nif__end_index) != len(self.nif__is_string):
    #             raise ValueError(
    #                 'Begin and end indices are provided ({}), '
    #                 'but do not fit the provided string (length = {})'
    #                 '.'.format((self.nif__begin_index, self.nif__end_index),
    #                            len(self.nif__is_string)))
    #     if self.nif__anchor_of is not None:
    #         ref_substring = self.reference_context.nif__is_string[
    #                         int(self.nif__begin_index):int(self.nif__end_index)]
    #         # Extractor returns different capitalization in matches!
    #         if self.nif__anchor_of.toPython().lower() != ref_substring.lower():
    #             raise ValueError(
    #                 'Anchor should be equal exactly to the subtring of '
    #                 'the reference context. You have anchor = "{}", '
    #                 'substring in ref context = "{}"'.format(
    #                     self.nif__anchor_of, ref_substring))
    #
    # @classmethod
    # def from_triples(cls, rdf_graph, ref_cxt):
    #     kwargs = dict()
    #     other_triples = rdflib.Graph()
    #     for s, p, o in rdf_graph:
    #         if p == nif_ns.beginIndex:
    #             kwargs['begin_index'] = int(o.toPython())
    #         elif p == nif_ns.endIndex:
    #             kwargs['end_index'] = int(o.toPython())
    #         elif p == nif_ns.referenceContext:
    #             ref_cxt_uriref = o
    #             assert ref_cxt_uriref == ref_cxt.uri, \
    #                 (ref_cxt_uriref, ref_cxt.uri)
    #         elif p == nif_ns.anchorOf:
    #             kwargs['anchor_of'] = o.toPython()
    #         else:
    #             other_triples.add((s, p, o))
    #     # uri_prefix = s.toPython()
    #     kwargs['begin_end_index'] = kwargs['begin_index'], kwargs['end_index']
    #     del kwargs['begin_index']
    #     del kwargs['end_index']
    #     out = cls(reference_context=ref_cxt, **kwargs)
    #     out += other_triples
    #     return out


if __name__ == '__main__':

    gen = TextAnnotation('http://qurator-project.de/res/12345678#offset_0_45',
                            text='Summary or translation of the document',
                            language='EN',
                            types=['qont:Summary']
                         )
    print(gen.toJSON())


