
from transformers import MarianMTModel, MarianTokenizer
from nltk.tokenize import sent_tokenize
from nltk.tokenize import LineTokenizer
import math
import torch

src_text = [
	'>>fr<< this is a sentence in english that we want to translate to french',
	'>>pt<< This should go to portuguese',
	'>>es<< And this to Spanish'
]

models = {}
tokenizers = {}

def detect_objects():
	model_name = 'Helsinki-NLP/opus-mt-en-ROMANCE'
	model_names = {}
	model_names['ende'] = 'Helsinki-NLP/opus-mt-en-de'
	model_names['deen'] = 'Helsinki-NLP/opus-mt-de-en'
	#model_names['enes'] = 'Helsinki-NLP/opus-mt-en-ROMANCE'
	#model_names['esen'] = 'Helsinki-NLP/opus-mt-ROMANCE-en'
	#model_names['enar'] = 'Helsinki-NLP/opus-mt-en-ar'
	#model_names['aren'] = 'Helsinki-NLP/opus-mt-ar-en'
	#model_names['enru'] = 'Helsinki-NLP/opus-mt-en-ru'
	#model_names['ruen'] = 'Helsinki-NLP/opus-mt-ru-en'
	for k, v in model_names.items():
		print(k, '->', v)
		tokenizer = MarianTokenizer.from_pretrained(v)
		print(tokenizer.supported_language_codes)
		model = MarianMTModel.from_pretrained(v)
		tokenizers[k] = tokenizer
		models[k] = model

def translate(text, orig_lang, targ_lang):
	model_name = orig_lang+targ_lang
	print('Model: ',model_name)
	if model_name in models:
		print('Model exists')
		model = models[model_name]
		tokenizer = tokenizers[model_name]
		src_text = ['>>'+targ_lang+'<< '+text]
		#translated = model.generate(**tokenizer.prepare_translation_batch(src_text))
		translated = model.generate(**tokenizer(src_text, return_tensors="pt", padding=True))
		tgt_text = [tokenizer.decode(t, skip_special_tokens=True) for t in translated]
		print(tgt_text)
		return tgt_text
	print('Error: languages ',orig_lang,'-->',targ_lang,' are not supported.')

def translate2(text, orig_lang, targ_lang):
    model_name = orig_lang+targ_lang
    print('Model: ',model_name)
    if model_name in models:
        print('Model exists')
        model = models[model_name]
        tokenizer = tokenizers[model_name]
        #src_text = ['>>'+targ_lang+'<< '+text]
        ##translated = model.generate(**tokenizer.prepare_translation_batch(src_text))
        #translated = model.generate(**tokenizer(src_text, return_tensors="pt", padding=True))
        #tgt_text = [tokenizer.decode(t, skip_special_tokens=True) for t in translated]
        #print(tgt_text)
        #return tgt_text

        if torch.cuda.is_available():  
            dev = "cuda"
        else:  
            dev = "cpu" 
            device = torch.device(dev)
 
        model.to(device)

        lt = LineTokenizer()
        batch_size = 8
        #paragraphs = lt.tokenize(src_text)   
        paragraphs = lt.tokenize(text)   
        translated_paragraphs = []

        for paragraph in paragraphs:
            sentences = sent_tokenize(paragraph)
            batches = math.ceil(len(sentences) / batch_size)     
            translated = []
            for i in range(batches):
                sent_batch = sentences[i*batch_size:(i+1)*batch_size]
                model_inputs = tokenizer(sent_batch, return_tensors="pt", padding=True, truncation=True, max_length=500).to(device)
                with torch.no_grad():
                    translated_batch = model.generate(**model_inputs)
                translated += translated_batch
            translated = [tokenizer.decode(t, skip_special_tokens=True) for t in translated]
            #print("\n\nPARAGRAPH: ")
            #print(translated)
            translated_paragraphs += [" ".join(translated)]

        translated_text = "\n".join(translated_paragraphs)
        return 0,translated_text
    return 1,"Model \""+model_name+"\" does not exist."

detect_objects()

#translate('This sentence must be translated into German.', 'en', 'de')
#translate('Ich bin in Berlin geboren, und ich will noch hier länger wohnen.', 'de', 'en')
#translate('This sentence must be translated into German.', 'en', 'es')
#translate('Vivo muy bien en mis circunstancias actuales. No necesito nada nuevo para ser feliz.', 'es', 'en')
