# Machine Translation Service

This service is based on the MarianMT modules (LINK) and uses the following translation models:

* Helsinki-NLP/opus-mt-en-de
* Helsinki-NLP/opus-mt-de-en
* Helsinki-NLP/opus-mt-en-ROMANCE
* Helsinki-NLP/opus-mt-ROMANCE-en

## Input

For the moment, it only accepts plain text at the input, which must be included in the POST request body. Apart from that, two URL parameters must be provided:

* orig_lang: original language of the text.
* targ_lang: target language into which the text must be translated.

## Output

For the moment, it only returns the text/plain translation.

## Endpoint

[POST] <server_url>/translate
