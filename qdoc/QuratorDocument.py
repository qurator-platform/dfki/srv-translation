#from QuratorAnnotation import QuratorAnnotation
#from QuratorAnnotationUnit import QuratorAnnotationUnit
from qdoc.QuratorDocumentPart import QuratorDocumentPart
from qdoc.BaseAnnotation import BaseAnnotation
from qdoc.TextAnnotation import TextAnnotation
from qdoc.PositionAnnotation import PositionAnnotation
from qdoc.LabelPositionAnnotation import LabelPositionAnnotation
from qdoc.Label import Label

import json
import uuid
import rdflib


class QuratorDocument:

    def __init__(self,
                 text,
                 uri=None,
                 types=None,
                 metadata=None,
                 annotations=None,
                 parts=None,
                 text_annotations=None):
        #if not NIFContext.is_context(context):
        #    raise TypeError('The provided context {} is not a NIFContext'
        #                    '.'.format(context))
        #self.rdf = rdflib.Graph()
        self.text = text
        self.offset_ini=0
        self.offset_end=len(text)
        self.id = ''
        if uri is None:
            uid = str(uuid.uuid4())
            self.id = "http://qurator-project.de/res/" + uid
        else:
            self.id = uri
        self.annotations = []
        if annotations is not None:
            for ann in annotations:
                self.add_annotation(ann)
        self.parts = []
        if parts is not None:
            for part in parts:
                self.add_part(part)
        self.text_annotations = []
        if text_annotations is not None:
            for text_annotation in text_annotations:
                self.add_text_annotation(text_annotation)
        ## self.rdf += self.context
        self.types = []
        self.types.append('qont:QuratorDocument')
        if types is not None:
            for typ in types:
                self.type.append(typ)
        if metadata is None:
            self.metadata=dict()
        else:
            self.metadata=metadata
        #self.validate()

    def validate(self):
        pass

    # @classmethod
    # def from_text(cls, text, uri="http://example.doc/" + str(uuid.uuid4())):
    #     cxt = NIFContext(is_string=text, uri_prefix=uri)
    #     return cls(context=cxt, annotations=[])

    def add_annotation(self, ann: BaseAnnotation):
        ann.update_reference_context(self.id)
        self.annotations.append(ann)
        try:
            self.validate()
        except (ValueError, TypeError) as e:
            self.annotations.pop()
            raise e
        return self

    def add_part(self, part: QuratorDocumentPart):
        part.update_reference_context(self.id)
        self.parts.append(part)
        try:
            self.validate()
        except (ValueError, TypeError) as e:
            self.parts.pop()
            raise e
        return self

    def add_text_annotation(self, ta: TextAnnotation):
        ta.update_reference_context(self.id)
        self.text_annotations.append(ta)
        try:
            self.validate()
        except (ValueError, TypeError) as e:
            self.text_annotations.pop()
            raise e
        return self

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)


    '''
    @property
    def rdf(self):
        _rdf = self.context
        for ann in self.annotations:
            _rdf += ann
            for au in ann.annotation_units.values():
                _rdf += au
        return _rdf

    def serialize(self, format="xml", uri_format=nif_ns.OffsetBasedString):
        if uri_format not in [nif_ns.OffsetBasedString, nif_ns.RFC5147String]:
            raise ValueError("Only RFC5147 and OffsetBased strings are "
                             "currently soported URI schemes")
        rdf_text = self.rdf.serialize(format=format)
    
        if uri_format == nif_ns.RFC5147String:
            RFC5147_str = br"#char\=\(\1,\2\)"
            offset_regex = br"#offset_(\d*)_(\d*)"
            rdf_text = re.sub(offset_regex, RFC5147_str, rdf_text)
        return rdf_text

    @classmethod
    def parse_rdf(cls, rdf_text, format="n3"):
        rdf_graph = rdflib.Graph()
        rdf_graph.parse(data=rdf_text, format=format)
    
        context_uri = rdf_graph.value(predicate=rdflib.RDF.type,
                                      object=nif_ns.Context)
        context_triples = rdf_graph.triples((context_uri, None, None))
        context = NIFContext.from_triples(context_triples)
    
        annotations = []
        struct_uris = list(rdf_graph[:nif_ns.referenceContext:context.uri])
        for struct_uri in struct_uris:
            struct_triples = rdf_graph.triples((struct_uri, None, None))
            struct = NIFAnnotation.from_triples(struct_triples, ref_cxt=context)
            au_uris = list(rdf_graph[struct_uri:nif_ns.annotationUnit:])
            for au_uri in au_uris:
                au_dict = {p_uri: o_uri for p_uri, o_uri in rdf_graph[au_uri::]}
                au = NIFAnnotationUnit(uri=au_uri, **au_dict)
                struct.add_annotation_unit(au)
            annotations.append(struct)
         out = cls(context=context, annotations=annotations)
    
         return out
    '''

    # def __copy__(self):
    #     return NIFDocument.parse_rdf(self.serialize(format='n3'))
    #
    # def __eq__(self, other):
    #     return isinstance(other, NIFDocument) and \
    #            self.serialize(format='n3') == other.serialize(format='n3')


if __name__ == '__main__':
    rdf_to_parse = '''
    @prefix dbo:   <http://dbpedia.org/ontology/> .
    @prefix geo:   <http://www.w3.org/2003/01/geo/wgs84_pos/> .
    @prefix dktnif: <http://dkt.dfki.de/ontologies/nif#> .
    @prefix nif-ann: <http://persistence.uni-leipzig.org/nlp2rdf/ontologies/nif-annotation#> .
    @prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
    @prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .
    @prefix itsrdf: <https://www.w3.org/2005/11/its/rdf-content/its-rdf.html#> .
    @prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
    @prefix nif:   <http://persistence.uni-leipzig.org/nlp2rdf/ontologies/nif-core#> .

    <http://dkt.dfki.de/documents#offset_11_17>
            a                     nif:RFC5147String , nif:String , nif:Structure ;
            nif:anchorOf          "Berlin" ;
            nif:beginIndex        "11"^^xsd:nonNegativeInteger ;
            nif:endIndex          "17"^^xsd:nonNegativeInteger ;
            nif:referenceContext  <http://dkt.dfki.de/documents#offset_0_26> ;
            itsrdf:taClassRef     dbo:Location ;
            itsrdf:taIdentRef     <http://www.wikidata.org/entity/Q64> ;
            itsrdf:taIdentRef     []  .

    <http://dkt.dfki.de/documents#offset_0_26>
            a               nif:RFC5147String , nif:String , nif:Context ;
            nif:beginIndex  "0"^^xsd:nonNegativeInteger ;
            nif:endIndex    "26"^^xsd:nonNegativeInteger ;
            nif:isString    "Welcome to Berlin in 2016." .
    '''
    #
    # print(f'Input: {rdf_to_parse}')
    # parsed = NIFDocument.parse_rdf(rdf_to_parse, format='turtle')
    # print(f'Parsed into NIFDocument')
    # context_str = parsed.context.nif__is_string
    # print(f'The nif:isString value: "{context_str}"')
    # structs = parsed.annotations
    # print(f'Number of annotations attached: {len(structs)}')
    # assert len(structs) == 1
    # ann = structs[0]
    # print(f'nif:anchorOf: "{ann.nif__anchor_of}", '
    #       f'itsrdf:taClassRef: "{ann.itsrdf__ta_class_ref}"')

    '''
    units1 = []
    unit11 = QuratorAnnotationUnit()
    unit11.add_property('itsrdf:taClassRef', 'http://wikidate.de/Person')
    units1.append(unit11)
    ann1 = PositionAnnotation('http://qurator-project.de/res/12345678#offset_0_45', 16, 22,
                            anchor_of='Berlin', properties=units1)
    '''
    
    labels = []
    properties = dict()
    properties['itsrdf:taClassRef']='Location'
    properties['itsrdf:tiIdentRef']='http://dbpedia.de/Berlin'
    label = Label(id='label1',properties=properties)
    labels.append(label)
    label_position_annotation = LabelPositionAnnotation('http://qurator-project.de/res/12345678#offset_0_45',
                              16,
                              22,
                              anchor_of='The capital of Germany is Berlin',
                              labels=labels,
                              title="2.1 Article 2")

    ta1 = TextAnnotation(None,
                            text='Summary or translation of the document',
                            language='en')
    ta1.add_type('qont:Translation')

    part1 = QuratorDocumentPart('http://qurator-project.de/res/12345678#offset_0_45',
                              10,
                              22,
                              'The capital of Germany is Berlin',
                              title="2.1 Article 2")

    q_document = QuratorDocument('The capital of Germany is Berlin')
    q_document.add_annotation(label_position_annotation)
    q_document.add_text_annotation(ta1)
    q_document.add_part(part1)

    print(q_document.toJSON())
