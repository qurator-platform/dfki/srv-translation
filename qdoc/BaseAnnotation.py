import re
import uuid
from typing import List
import rdflib
import json


class BaseAnnotation():
    # nif_classes = [nif_ns.Annotation]

    def __init__(self,
                 reference_context,
                 source=None,
                 metadata=None,
                 #annotation_units : List[QuratorAnnotationUnit] = None,
                 **kwargs):
        #Map < String, Object > metadata = new HashMap < String, Object > ();

        self.type = []
        self.type.append('nif:Annotation')
        self.type.append('qont: BaseAnnotation')
        self.reference_context = reference_context
        self.source = source
        self.metadata = metadata
        #self.annotation_units = []
        #if annotation_units is not None:
        #    for au in annotation_units:
        #        self.add_annotation_unit(au)
        self.id = self.getId()
        # self.validate()

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

    def getId(self):
        #if '#' in self.reference_context:
        #    short_context = self.reference_context.split('#')[0]
        #    return short_context + "#offset_" + str(self.offset_ini) + "_" + str(self.offset_end)
        #else:
        #    return self.reference_context + "#offset_" + str(self.offset_ini) + "_" + str(self.offset_end)
        return self.reference_context

    @staticmethod
    def is_annotation(cxt):
        return isinstance(cxt, BaseAnnotation)

    def add_metadata(self, au):
        self.metadata.append(au)

    def update_reference_context(self, reference_context):
        self.reference_context = reference_context
        self.id = self.getId()

    # def remove_annotation_unit(self, au_uri: str):
    #     self.delattr('nif__annotation_unit', au_uri)
    #     del self.annotation_units[au_uri]

    # def validate(self):
    #     if self.reference_context is not None:
    #         if not NIFContext.is_context(self.reference_context):
    #             raise ValueError(
    #                 'The provided reference context is not compatible with '
    #                 'nif.Context class.')
    #     if self.nif__is_string is not None:
    #         if not isinstance(self.nif__is_string, str):
    #             raise TypeError('is_string value {} should be '
    #                             'a string'.format(self.nif__is_string))
    #         if int(self.nif__begin_index) != 0 or \
    #                 int(self.nif__end_index) != len(self.nif__is_string):
    #             raise ValueError(
    #                 'Begin and end indices are provided ({}), '
    #                 'but do not fit the provided string (length = {})'
    #                 '.'.format((self.nif__begin_index, self.nif__end_index),
    #                            len(self.nif__is_string)))
    #     if self.nif__anchor_of is not None:
    #         ref_substring = self.reference_context.nif__is_string[
    #                         int(self.nif__begin_index):int(self.nif__end_index)]
    #         # Extractor returns different capitalization in matches!
    #         if self.nif__anchor_of.toPython().lower() != ref_substring.lower():
    #             raise ValueError(
    #                 'Anchor should be equal exactly to the subtring of '
    #                 'the reference context. You have anchor = "{}", '
    #                 'substring in ref context = "{}"'.format(
    #                     self.nif__anchor_of, ref_substring))
    #
    # @classmethod
    # def from_triples(cls, rdf_graph, ref_cxt):
    #     kwargs = dict()
    #     other_triples = rdflib.Graph()
    #     for s, p, o in rdf_graph:
    #         if p == nif_ns.beginIndex:
    #             kwargs['begin_index'] = int(o.toPython())
    #         elif p == nif_ns.endIndex:
    #             kwargs['end_index'] = int(o.toPython())
    #         elif p == nif_ns.referenceContext:
    #             ref_cxt_uriref = o
    #             assert ref_cxt_uriref == ref_cxt.uri, \
    #                 (ref_cxt_uriref, ref_cxt.uri)
    #         elif p == nif_ns.anchorOf:
    #             kwargs['anchor_of'] = o.toPython()
    #         else:
    #             other_triples.add((s, p, o))
    #     # uri_prefix = s.toPython()
    #     kwargs['begin_end_index'] = kwargs['begin_index'], kwargs['end_index']
    #     del kwargs['begin_index']
    #     del kwargs['end_index']
    #     out = cls(reference_context=ref_cxt, **kwargs)
    #     out += other_triples
    #     return out



if __name__ == '__main__':
    rdf_to_parse = '''
    @prefix dbo:   <http://dbpedia.org/ontology/> .
    @prefix geo:   <http://www.w3.org/2003/01/geo/wgs84_pos/> .
    @prefix dktnif: <http://dkt.dfki.de/ontologies/nif#> .
    @prefix nif-ann: <http://persistence.uni-leipzig.org/nlp2rdf/ontologies/nif-annotation#> .
    @prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
    @prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .
    @prefix itsrdf: <https://www.w3.org/2005/11/its/rdf-content/its-rdf.html#> .
    @prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
    @prefix nif:   <http://persistence.uni-leipzig.org/nlp2rdf/ontologies/nif-core#> .

    <http://dkt.dfki.de/documents#offset_11_17>
            a                     nif:RFC5147String , nif:String , nif:Structure ;
            nif:anchorOf          "Berlin" ;
            nif:beginIndex        "11"^^xsd:nonNegativeInteger ;
            nif:endIndex          "17"^^xsd:nonNegativeInteger ;
            nif:referenceContext  <http://dkt.dfki.de/documents#offset_0_26> ;
            itsrdf:taClassRef     dbo:Location ;
            itsrdf:taIdentRef     <http://www.wikidata.org/entity/Q64> ;
            itsrdf:taIdentRef     []  .

    <http://dkt.dfki.de/documents#offset_0_26>
            a               nif:RFC5147String , nif:String , nif:Context ;
            nif:beginIndex  "0"^^xsd:nonNegativeInteger ;
            nif:endIndex    "26"^^xsd:nonNegativeInteger ;
            nif:isString    "Welcome to Berlin in 2016." .
    '''
    #
    # print(f'Input: {rdf_to_parse}')
    # parsed = NIFDocument.parse_rdf(rdf_to_parse, format='turtle')
    # print(f'Parsed into NIFDocument')
    # context_str = parsed.context.nif__is_string
    # print(f'The nif:isString value: "{context_str}"')
    # structs = parsed.annotations
    # print(f'Number of annotations attached: {len(structs)}')
    # assert len(structs) == 1
    # ann = structs[0]
    # print(f'nif:anchorOf: "{ann.nif__anchor_of}", '
    #       f'itsrdf:taClassRef: "{ann.itsrdf__ta_class_ref}"')

    ##units = []

    ##unit1 = QuratorAnnotationUnit()
    ##unit1.add_property('itsrdf:taClassRef','http://wikidate.de/Person')
    ##units.append(unit1)
    ##unit2 = QuratorAnnotationUnit()
    ##units.append(unit2)
    ##unit3 = QuratorAnnotationUnit()
    ##units.append(unit3)

    #ann = QuratorAnnotation(0, 25, 'http://qurator-project.de/res/12345678', 'The capital of Germany is Berlin')
    ann = BaseAnnotation('http://qurator-project.de/res/12345678#offset_0_45')

    #s = json.dumps(ann.serializable_attrs2())
    #print(s)
    #s = json.dumps(ann.__dict__)
    #print(s)
    #s = ann.serializable_attrs()
    #print(s)
    print(ann.toJSON())


