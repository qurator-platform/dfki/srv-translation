import re
import uuid
from typing import List
import rdflib
import json
from qdoc.BaseAnnotation import BaseAnnotation


class PositionAnnotation(BaseAnnotation):
    # nif_classes = [nif_ns.Annotation]
    def __init__(self,
                 reference_context,
                 begin_index,
                 end_index,
                 anchor_of=None,
                 types = None,
                 source=None,
                 metadata=None,
                 **kwargs):
        self.offset_ini = begin_index
        self.offset_end = end_index
        self.anchor_of = anchor_of
        super().__init__(reference_context,
                         source=source,
                         metadata=metadata,
                         **kwargs)
        self.type = []
        self.type.append('qont:PositionAnnotation')
        if types is not None:
            for typ in types:
                self.type.append(typ)
        # self.validate()

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

    def getId(self):
        if '#' in self.reference_context:
            short_context = self.reference_context.split('#')[0]
            return short_context + "#offset_" + str(self.offset_ini) + "_" + str(self.offset_end)
        else:
            return self.reference_context + "#offset_" + str(self.offset_ini) + "_" + str(self.offset_end)

    @staticmethod
    def is_position_annotation(cxt):
        return isinstance(cxt, PositionAnnotation)

    # def remove_annotation_unit(self, au_uri: str):
    #     self.delattr('nif__annotation_unit', au_uri)
    #     del self.annotation_units[au_uri]

    # def validate(self):
    #     if self.reference_context is not None:
    #         if not NIFContext.is_context(self.reference_context):
    #             raise ValueError(
    #                 'The provided reference context is not compatible with '
    #                 'nif.Context class.')
    #     if self.nif__is_string is not None:
    #         if not isinstance(self.nif__is_string, str):
    #             raise TypeError('is_string value {} should be '
    #                             'a string'.format(self.nif__is_string))
    #         if int(self.nif__begin_index) != 0 or \
    #                 int(self.nif__end_index) != len(self.nif__is_string):
    #             raise ValueError(
    #                 'Begin and end indices are provided ({}), '
    #                 'but do not fit the provided string (length = {})'
    #                 '.'.format((self.nif__begin_index, self.nif__end_index),
    #                            len(self.nif__is_string)))
    #     if self.nif__anchor_of is not None:
    #         ref_substring = self.reference_context.nif__is_string[
    #                         int(self.nif__begin_index):int(self.nif__end_index)]
    #         # Extractor returns different capitalization in matches!
    #         if self.nif__anchor_of.toPython().lower() != ref_substring.lower():
    #             raise ValueError(
    #                 'Anchor should be equal exactly to the subtring of '
    #                 'the reference context. You have anchor = "{}", '
    #                 'substring in ref context = "{}"'.format(
    #                     self.nif__anchor_of, ref_substring))
    #
    # @classmethod
    # def from_triples(cls, rdf_graph, ref_cxt):
    #     kwargs = dict()
    #     other_triples = rdflib.Graph()
    #     for s, p, o in rdf_graph:
    #         if p == nif_ns.beginIndex:
    #             kwargs['begin_index'] = int(o.toPython())
    #         elif p == nif_ns.endIndex:
    #             kwargs['end_index'] = int(o.toPython())
    #         elif p == nif_ns.referenceContext:
    #             ref_cxt_uriref = o
    #             assert ref_cxt_uriref == ref_cxt.uri, \
    #                 (ref_cxt_uriref, ref_cxt.uri)
    #         elif p == nif_ns.anchorOf:
    #             kwargs['anchor_of'] = o.toPython()
    #         else:
    #             other_triples.add((s, p, o))
    #     # uri_prefix = s.toPython()
    #     kwargs['begin_end_index'] = kwargs['begin_index'], kwargs['end_index']
    #     del kwargs['begin_index']
    #     del kwargs['end_index']
    #     out = cls(reference_context=ref_cxt, **kwargs)
    #     out += other_triples
    #     return out


if __name__ == '__main__':
    position_annotation = PositionAnnotation('http://qurator-project.de/res/12345678#offset_0_45',
                              16,
                              22,
                              anchor_of='The capital of Germany is Berlin',
                              title="2.1 Article 2")
    print(position_annotation.toJSON())

