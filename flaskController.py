#!/usr/bin/python3
from flask import Flask, Response, flash, redirect, request, url_for
from flask_cors import CORS
import os
import json
from werkzeug.utils import secure_filename
from flask import jsonify
from qdoc.QuratorDocument import QuratorDocument
from qdoc.TextAnnotation import TextAnnotation

# custom modules
import translation

"""
to start run:
export FLASK_APP=flaskController.py
export FLASK_DEBUG=1 (optional, to reload upon changes automatically)
python -m flask run

example calls:

curl -X GET localhost:5000/detectLanguages?input="aap"
curl -F 'pdffile=@temp_pdf_storage/machine_readable_single_column_2.pdf' -X POST localhost:5000/ocr
"""

app = Flask(__name__)
app.secret_key = "super secret key"
CORS(app)

@app.route('/welcome', methods=['GET'])
def dummy():
    return "Hello stranger, can you tell us where you've been?\nMore importantly, how ever did you come to be here?\n"

##################### object detection #####################
@app.route('/translate', methods=['POST'])
def translateText():
    ctype = request.headers["Content-Type"]
    print("Content type: {}".format(ctype))
    accept = request.headers["Accept"]
    print("Response type: {}".format(accept))
    if not request.data:
        print("Error: No request body provided in message")
        return Response(response=translated[0], status=200, content_type='text/plain')
    request_body = request.data.decode('utf-8')
    #if ctype=="text/plain":
    if "text/plain" in ctype:
        q_document = QuratorDocument(request_body)
    elif ctype=="text/turtle":
        q_document = QuratorDocument.fromJSON(request_body)
    else:
        print("Error: Content-Type {} is not supported".format(ctype))
        return Response(response="Error: Content-Type {} is not supported".format(ctype), status=404, content_type='text/plain')
    orig_lang = request.args.get('orig_lang')
    targ_lang = request.args.get('targ_lang')

    translated = translation.translate2(request_body, orig_lang, targ_lang)
    print(translated)
    print(json.dumps(translated))
    #if accept=="text/plain":
    if "text/plain" in accept:
        return Response(response=translated[0], status=200, content_type='text/plain')
    elif accept=="text/turtle":
        ta1 = TextAnnotation(None,
                            text=translated,
                            language=targ_lang)
        ta1.add_type('qont:Translation')
        q_document.add_text_annotation(ta1)
        return Response(response=q_document.toJSON(), status=200, content_type=accept)
    else:
        print("Error: Accept {} is not supported".format(accept))
        return Response(response="Error: Accept {} is not supported".format(accept), status=404, content_type='text/plain')

    #return Response(response=json.dumps(translated), status=200, content_type='text/plain')
    #return Response(response='salida', status=200, content_type='text/plain')

@app.route('/translateForm', methods=['POST'])
def translateTextFromForm():
    ctype = request.headers["Content-Type"]
    print("Content type: {}".format(ctype))
    accept = request.headers["Accept"]
    print("Response type: {}".format(accept))

    if not request.form:
        msg = "Error: No request FORM provided in message"
        print(msg)
        return Response(response=msg, status=404, content_type='text/plain')
    #request_body = request.data.decode('utf-8')
    request_text = request.form.get("text")
    request_format = request.form.get("format")
    print("Obtained text" + request_text)
    #'''
    #if request_format=="text/plain":
    if "text/plain" in request_format:
        q_document = QuratorDocument(request_text)
    elif request_format=="text/turtle":
        q_document = QuratorDocument.fromJSON(request_text)
    else:
        print("Error: Content-Type {} is not supported".format(ctype))
        return Response(response="Error: Content-Type {} is not supported".format(ctype), status=404, content_type='text/plain')
    orig_lang = request.args.get('orig_lang')
    targ_lang = request.args.get('targ_lang')

    value,translated = translation.translate2(request_text, orig_lang, targ_lang)
    if value==1:
        return Response(response=translated, status=500, content_type='text/plain')
    #print("TRANSLATED TEXT: \n")
    #print(translated)
    #print("\nTRANSLATED TEXT DUMP: ", translated)
    #print(json.dumps(translated))
    #if accept=="text/plain":
    if "text/plain" in accept:
        #return Response(response=translated[0], status=200, content_type='text/plain')
        return Response(response=translated, status=200, content_type='text/plain')
    elif accept=="text/turtle":
        ta1 = TextAnnotation(None,
                            text=translated,
                            language=targ_lang)
        ta1.add_type('qont:Translation')
        q_document.add_text_annotation(ta1)
        return Response(response=q_document.toJSON(), status=200, content_type=accept)
    else:
        print("Error: Accept {} is not supported".format(accept))
        return Response(response="Error: Accept {} is not supported".format(accept), status=404, content_type='text/plain')

    #return Response(response=json.dumps(translated), status=200, content_type='text/plain')
    #return Response(response='salida', status=200, content_type='text/plain')
    #'''
    #return Response(response="Testing output", status=404, content_type='text/plain')

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 8080))
    app.run(host='localhost', port=port, debug=True)
    #dict = objdet.detect_objects('dog1.jpg')

